import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  dataJSON:any;
  response:any;
  
  constructor( public auth: AuthService, private router:Router) {}

  ngOnInit(): void {
    this.dataJSON = this.auth.getJSON();
    this.data();
  }
  
  data(){
    this.response = this.dataJSON.entries;
    debugger
  }
  
  buscar(buscarText:string){
    this.router.navigate( ['/buscador',buscarText] );
  }


}
