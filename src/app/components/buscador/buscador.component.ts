import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { HomeComponent } from '../home/home.component';
import { Router } from '@angular/router';



@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  palabras:any[] = [];
  palabraBucada: any;

  constructor( private activateRoute:ActivatedRoute, private home:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.activateRoute.params.subscribe( params => {
      this.palabraBucada = params['buscarText'];
      this.palabras = this.home.buscarPalabra(params['buscarText']);
      console.log(this.palabras);
    });
  }

  buscar(buscarText:string){
    this.router.navigate( ['/buscador',buscarText] );
  }

}
